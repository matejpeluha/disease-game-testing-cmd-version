package diseaseGame.backend;

import diseaseGame.backend.parameters.Date;
import diseaseGame.backend.parameters.Population;
import diseaseGame.backend.parameters.actions.AllActions;
import diseaseGame.backend.parameters.actions.allActions.Action;
import diseaseGame.backend.parameters.disasters.AllDisasters;
import diseaseGame.backend.parameters.disasters.allDisasters.Disaster;
import diseaseGame.backend.parameters.diseases.Disease;
import diseaseGame.backend.parameters.diseases.Virus;
import lombok.Getter;

@Getter
public abstract class Simulation {
    protected Disease disease;
    protected Population population;
    protected Date date;
    protected AllActions allActions;
    protected AllDisasters allDisasters;

    final protected int DEFAULT_ACTION_POINTS = 5;
    protected int actualPoints;

    public Simulation(){
        defaultSetUp();
    }

    private void defaultSetUp(){
        final long defaultPopulationSize = 2 * Population.MIN_POPULATION_SIZE;
        this.population = new Population(defaultPopulationSize);

        this.disease = new Virus("SARS-CoV-2");

        this.date = new Date();
        this.date.setCurrentDate();

        this.allActions = new AllActions();
        this.allDisasters = new AllDisasters();

        this.actualPoints = DEFAULT_ACTION_POINTS;
    }

    public String getTodayDisasterWarning(){
        int day = date.getDiseaseActiveDay();
        return allDisasters.getDisasterWarning(day);
    }

    public Disaster getTodayDisaster(){
        int day = date.getDiseaseActiveDay();
        return allDisasters.getDisaster(day);
    }

    private void deactivateDisaster(){
        int today = date.getDiseaseActiveDay();
        this.allDisasters.deactivateTodayDisaster(today);
    }

    private void payForDisaster(){
        Disaster disaster = getTodayDisaster();
        int cost = disaster.getCOST();

        this.actualPoints -= cost;

        deactivateDisaster();
    }

    public void dealWithDisaster(String decision)throws IllegalArgumentException{
        if(decision == null)
            throw new IllegalArgumentException("Neplatny prikaz");
        else if(decision.equals("yes"))
            payForDisaster();
        else
            throw new IllegalArgumentException("Neplatny prikaz.");
    }

    private void getBackPoints(int index) throws IllegalAccessException {
        Action action = allActions.getAction(index);
        int actionCost = action.getCost();

        this.actualPoints += actionCost;
    }

    private void boostDisease(int index) throws IllegalAccessException {
        Action action = allActions.getAction(index);
        double changeTransferRate = action.getTransferRateBlock();
        double changeRecoverRate = action.getRecoverRateBlock();

        this.disease.boostTransferRateBy(changeTransferRate);
        this.disease.boostRecoverRateBy(changeRecoverRate);
    }

    private void turnOffAction(int index) throws IllegalAccessException {
        this.allActions.turnOff(index);

        getBackPoints(index);

        boostDisease(index);
    }

    private void spendPoints(int index) throws IllegalAccessException, RuntimeException {
        Action action = allActions.getAction(index);
        int cost = action.getCost();

        if(cost > actualPoints)
            throw new RuntimeException("Nedostatok bodov na zakupenie akcie: " + index);

        this.actualPoints -= cost;
    }

    private void weakenDisease(int index) throws IllegalAccessException {
        Action action = allActions.getAction(index);
        double changeTransferRate = action.getTransferRateBlock();
        double changeRecoverRate = action.getRecoverRateBlock();

        this.disease.weakenTransferRateBy(changeTransferRate);
        this.disease.weakenRecoverRateBy(changeRecoverRate);
    }

    private void turnOnAction(int index) throws IllegalAccessException, RuntimeException {
        spendPoints(index);

        this.allActions.turnOn(index);

        weakenDisease(index);
    }

    public void turnAction(int index) throws IllegalAccessException, RuntimeException {
        if(allActions.isActionActive(index))
            turnOffAction(index);
        else
            turnOnAction(index);
    }

    private void doDisaster(Disaster disaster){
        double changeTransferRate = disaster.getChangeTransferRate();
        double changeRecoverRate = disaster.getChangeRecoverRate();

        this.disease.boostTransferRateBy(changeTransferRate);
        this.disease.boostRecoverRateBy(changeRecoverRate);
    }

    private void doActiveDisaster(){
        Disaster disaster = getTodayDisaster();

        if(getTodayDisaster().isActive())
            doDisaster(disaster);
    }

    private long getNumberOfNewInfectious(){
        return (long) ((double)population.getInfectious() * disease.getTransferRate());
    }

    private long getNumberOfNewRecovers(){
        return (long) ((double)population.getInfectious() * disease.getRecoverRate());
    }

    private void redistributePopulation(){
        long newInfectious = getNumberOfNewInfectious();
        this.population.moveSusceptibleToInfectious(newInfectious);

        long newRecovers = getNumberOfNewRecovers();
        this.population.moveInfectiousToRecovered(newRecovers);
    }

    public void nextDay(){
        doActiveDisaster();

        this.date.setNextDay();

        redistributePopulation();

        this.actualPoints += DEFAULT_ACTION_POINTS;

        this.disease.mutate();
    }

    public boolean isPlayerWinner(){
        double diseaseR0 = disease.getR0();
        long infectious = population.getInfectious();

        return diseaseR0 < 1 || infectious == 0;
    }

    public boolean isDiseaseWinner(){
        long populationSize = population.getPopulationSize();
        long allInfectedPopulation = population.getAllInfected();
        double rateOfInfectedPopulation = population.getAllInfectedRate();

        return rateOfInfectedPopulation >= 0.9;
    }

    public boolean somebodyIsNOTWinner(){
        return !(isDiseaseWinner() || isPlayerWinner());
    }

    public void setPopulation(Population population){
        this.population = population;
    }

    public void setDisease(Disease disease) {
        this.disease = disease;
    }

    public Action getAction(int index) throws IllegalAccessException {
        return allActions.getAction(index);
    }
}
