package diseaseGame.backend;

import diseaseGame.backend.parameters.Vax;
import lombok.Getter;

public class VaxSimulation extends Simulation{
    @Getter private final Vax vax;

    public VaxSimulation(){
        super();
        this.vax = new Vax();
    }

    public int getVaxingPerDay(){
        return vax.getVaxRate();
    }

    public int getCostOfNextVaxLevel(){
        return vax.getNextLevel();
    }

    private void throwExceptionIfNotEnoughPoints(int cost)throws RuntimeException{
        if(cost > actualPoints)
            throw new RuntimeException("Nedostatok penazi na zlepsenie vakciny");
    }

    private void throwExceptionIfVaxOnMaxLevel(){
        int vaxLevel = vax.getVaxRate();
        int maxLevel = vax.getMAX_LEVEL();
        if(vaxLevel == maxLevel)
            throw new RuntimeException("Dosiahnuty najvyssi level ockovania.");
    }

    public void levelUpVaxRate() throws RuntimeException, IllegalAccessException {
        int cost = getCostOfNextVaxLevel();
        throwExceptionIfNotEnoughPoints(cost);

        throwExceptionIfVaxOnMaxLevel();

        this.actualPoints -= cost;
        this.vax.levelUpVaxRate();
    }

    @Override
    public void nextDay() {
        int vaxRate = vax.getVaxRate();
        this.population.moveSusceptibleToRecovered(vaxRate);

        super.nextDay();
    }
}
