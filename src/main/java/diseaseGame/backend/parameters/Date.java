package diseaseGame.backend.parameters;

import lombok.Getter;

import java.util.Calendar;
import java.util.GregorianCalendar;

@Getter
public class Date {
    private int year;
    private int month;
    private int day;
    private int diseaseActiveDay;

    public void setCurrentDate(){
        this.year = Calendar.getInstance().get(Calendar.YEAR);
        this.month = Calendar.getInstance().get(Calendar.MONTH);
        this.day = Calendar.getInstance().get(Calendar.DATE);
        this.diseaseActiveDay = 1;
    }

    private boolean isLeapYearFebruary(){
        GregorianCalendar calendar = new GregorianCalendar();

        return calendar.isLeapYear(year) && month == 2;
    }

    private boolean is30DaysMonth(){
        return month == 4 || month == 6 || month == 9 || month == 11;
    }

    private boolean is31DaysMonth(){
        return month == 1 || month == 3 || month == 5 || month ==7 || month == 8 || month == 10 || month == 12;
    }

    private boolean isLastDayOfMonth(){
        if(isLeapYearFebruary() && day == 29)
            return true;
        else if(!isLeapYearFebruary() && day == 28)
            return true;
        else if(is30DaysMonth() && day == 30)
            return true;
        else if(is31DaysMonth() && day == 31)
            return true;
        else
            return false;
    }

    private void setNextYear(){
        this.year++;
    }

    private void setNextMonth(){
        if(this.month == 12){
            setNextYear();
            this.month = 1;
        }
        else
            this.month++;
    }

    public void setNextDay(){
        if(isLastDayOfMonth()) {
            setNextMonth();
            this.day = 1;
        }
        else
            this.day++;

        this.diseaseActiveDay++;
    }

}
