package diseaseGame.backend.parameters;

import lombok.Getter;

@Getter
public class Population {
    //SIR MODEL
    private long susceptible;
    private long infectious;
    private long recovered;

    private long allInfected;

    static public final long MIN_POPULATION_SIZE = 1000000L;

    public Population(long populationSize) throws IllegalArgumentException{
        if(populationSize < MIN_POPULATION_SIZE)
            throw new IllegalArgumentException("Prilis mala populacia");

        this.infectious = 1;
        this.susceptible = populationSize - infectious;
        this.allInfected = infectious;
        this.recovered = 0;
    }

    public void moveSusceptibleToInfectious(long numberOfPeople){
        if(susceptible <= numberOfPeople)
            numberOfPeople = susceptible;

        this.susceptible -= numberOfPeople;
        this.infectious += numberOfPeople;
        this.allInfected += numberOfPeople;
    }

    public void moveInfectiousToRecovered(long numberOfPeople){
        if(infectious <= numberOfPeople)
            numberOfPeople = infectious;

        this.infectious -= numberOfPeople;
        this.recovered += numberOfPeople;
    }

    public void moveSusceptibleToRecovered(long numberOfPeople){
        if(susceptible <= numberOfPeople)
            numberOfPeople = susceptible;

        this.susceptible -= numberOfPeople;
        this.recovered += numberOfPeople;
    }

    public long getPopulationSize(){
        return susceptible + infectious + recovered;
    }

    public double getAllInfectedRate(){
        return (double) allInfected / getPopulationSize();
    }
}
