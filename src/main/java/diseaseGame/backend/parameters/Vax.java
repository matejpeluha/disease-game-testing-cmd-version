package diseaseGame.backend.parameters;

import lombok.Getter;

public class Vax {
    private final int[] LEVELS;
    @Getter private final int MAX_LEVEL;
    @Getter private int nextLevel = 1;
    @Getter private int vaxRate;

    public Vax(){
        this.LEVELS = new int[]{0, 100, 1000, 10000, 50000, 200000};
        this.MAX_LEVEL = LEVELS.length - 1;
        this.vaxRate = LEVELS[0];
    }

    public void levelUpVaxRate() throws IllegalAccessException {
        if(nextLevel > MAX_LEVEL)
            throw new IllegalAccessException("Vakcína je na najvyššej úrovni.");

        this.vaxRate = LEVELS[nextLevel];

        if(nextLevel < MAX_LEVEL)
            this.nextLevel++;
    }


}
