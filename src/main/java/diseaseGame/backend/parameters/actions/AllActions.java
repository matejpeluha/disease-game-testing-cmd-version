package diseaseGame.backend.parameters.actions;

import diseaseGame.backend.parameters.actions.allActions.Action;
import diseaseGame.backend.parameters.actions.allActions.AllActionsArrayCreator;
import lombok.Getter;


@Getter
public class AllActions {
    private final Action[] allActions;
    private final int NUMBER_OF_ACTIONS;

    public AllActions(){
        AllActionsArrayCreator creator = new AllActionsArrayCreator();

        this.allActions = creator.createArrayWithAllActions();

        this.NUMBER_OF_ACTIONS = allActions.length;

    }

    private void throwExceptionIfBadIndex(int index) throws IllegalAccessException {
        if(index < 0 || index >= NUMBER_OF_ACTIONS)
            throw new IllegalAccessException("Neplatny index");
    }

    public boolean isActionActive(int index) throws IllegalAccessException {
        throwExceptionIfBadIndex(index);

        Action action = allActions[index];

        return action.isActive();
    }

    public void turnOn(int index) throws IllegalAccessException {
        throwExceptionIfBadIndex(index);

        this.allActions[index].turnOn();
    }

    public void turnOff(int index) throws IllegalAccessException {
        throwExceptionIfBadIndex(index);

        this.allActions[index].turnOff();
    }

    public Action getAction(int index) throws IllegalAccessException {
        throwExceptionIfBadIndex(index);

        return allActions[index];
    }


}
