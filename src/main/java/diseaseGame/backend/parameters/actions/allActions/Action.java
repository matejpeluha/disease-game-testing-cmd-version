package diseaseGame.backend.parameters.actions.allActions;

import lombok.Getter;

@Getter
abstract public class Action {
    protected double transferRateBlock;
    protected double recoverRateBlock;
    protected int cost;
    protected String name;
    protected boolean active = false;

    public void turnOn() throws IllegalAccessException {
        if(active == true)
            throw new IllegalAccessException("Akcia uz je aktivna.");

        this.active = true;
    }

    public void turnOff() throws IllegalAccessException {
        if(active == false)
            throw new IllegalAccessException("Akcia uz je aktivna.");

        this.active = false;
    }
}
