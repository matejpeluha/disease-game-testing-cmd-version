package diseaseGame.backend.parameters.actions.allActions;

import diseaseGame.backend.parameters.actions.allActions.closes.*;
import diseaseGame.backend.parameters.actions.allActions.health.*;
import diseaseGame.backend.parameters.actions.allActions.limitations.*;
import diseaseGame.backend.parameters.actions.allActions.locks.*;
import diseaseGame.backend.parameters.actions.allActions.social.*;


public class AllActionsArrayCreator {
    public Action[] createArrayWithAllActions(){
        Action [] allActions = new Action[]{
                new CloseAirports(),
                new CloseBorders(),
                new CloseCulturalEvents(),
                new CloseFactories(),
                new CloseGroceries(),
                new CloseReligionPlaces(),
                new CloseRestaurants(),
                new CloseSchools(),
                new CloseShops(),
                new CloseSportEvents(),
                new BuildHospitalCamps(),
                new BuildSpecialHospitals(),
                new DisinfectCities(),
                new DrugStoreBoost(),
                new HomeTesting(),
                new HospitalBoost(),
                new MandatoryMouthCover(),
                new MassGraves(),
                new TemperatureMeasuring(),
                new BanGatherings(),
                new BanNightOutsideMovement(),
                new LimitedOutsideMovement(),
                new MaxLimitOfPeopleAtPlaces(),
                new QuarantineForIncomers(),
                new LockCities(),
                new LockRetirementHomes(),
                new FinesForEndangering(),
                new MobileGPSMovementMonitoring(),
                new PropagationDiseaseAwareness(),
                new SpecialPhoneHelpLink()};

        return allActions;
    }
}
