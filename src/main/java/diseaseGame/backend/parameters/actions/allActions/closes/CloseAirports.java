package diseaseGame.backend.parameters.actions.allActions.closes;

import diseaseGame.backend.parameters.actions.allActions.Action;

public class CloseAirports extends Action {
    public CloseAirports(){
        this.transferRateBlock = -0.4;
        this.recoverRateBlock = 0;
        this.cost = 3;
        this.name = "Uzavretie letísk";
    }
}
