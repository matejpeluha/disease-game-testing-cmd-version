package diseaseGame.backend.parameters.actions.allActions.closes;

import diseaseGame.backend.parameters.actions.allActions.Action;

public class CloseBorders extends Action {
    public CloseBorders(){
        this.transferRateBlock = -0.5;
        this.recoverRateBlock = 0;
        this.cost = 4;
        this.name = "Uzavretie hraníc";
    }
}
