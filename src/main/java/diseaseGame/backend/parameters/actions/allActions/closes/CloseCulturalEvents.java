package diseaseGame.backend.parameters.actions.allActions.closes;

import diseaseGame.backend.parameters.actions.allActions.Action;

public class CloseCulturalEvents extends Action {
    public CloseCulturalEvents(){
        this.transferRateBlock = -0.2;
        this.recoverRateBlock = 0;
        this.cost = 2;
        this.name = "Uzavretie kultúrnych udalostí";
    }
}
