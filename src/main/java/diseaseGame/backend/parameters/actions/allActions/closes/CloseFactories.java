package diseaseGame.backend.parameters.actions.allActions.closes;

import diseaseGame.backend.parameters.actions.allActions.Action;

public class CloseFactories extends Action {
    public CloseFactories(){
        this.transferRateBlock = -0.15;
        this.recoverRateBlock = 0;
        this.cost = 3;
        this.name = "Uzavretie tovární";
    }
}
