package diseaseGame.backend.parameters.actions.allActions.closes;

import diseaseGame.backend.parameters.actions.allActions.Action;

public class CloseGroceries extends Action {
    public CloseGroceries(){
        this.transferRateBlock = -0.2;
        this.recoverRateBlock = 0.01;
        this.cost = 3;
        this.name = "Uzavretie potravín";
    }
}
