package diseaseGame.backend.parameters.actions.allActions.closes;

import diseaseGame.backend.parameters.actions.allActions.Action;

public class CloseReligionPlaces extends Action {
    public CloseReligionPlaces(){
        this.transferRateBlock = -0.03;
        this.recoverRateBlock = 0.01;
        this.cost = 2;
        this.name = "Uzavretie náboženských miest";
    }
}
