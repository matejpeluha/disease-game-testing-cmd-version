package diseaseGame.backend.parameters.actions.allActions.closes;

import diseaseGame.backend.parameters.actions.allActions.Action;

public class CloseRestaurants extends Action {
    public CloseRestaurants(){
        this.transferRateBlock = -0.15;
        this.recoverRateBlock = 0;
        this.cost = 2;
        this.name = "Uzavretie reštaurácií";
    }
}
