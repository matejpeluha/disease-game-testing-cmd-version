package diseaseGame.backend.parameters.actions.allActions.closes;

import diseaseGame.backend.parameters.actions.allActions.Action;

public class CloseSchools extends Action {
    public CloseSchools(){
        this.transferRateBlock = -0.2;
        this.recoverRateBlock = 0.01;
        this.cost = 2;
        this.name = "Uzavretie škôl";
    }
}
