package diseaseGame.backend.parameters.actions.allActions.closes;

import diseaseGame.backend.parameters.actions.allActions.Action;

public class CloseShops extends Action {
    public CloseShops(){
        this.transferRateBlock = -0.1;
        this.recoverRateBlock = 0;
        this.cost = 3;
        this.name = "Uzavretie obchodov";
    }
}
