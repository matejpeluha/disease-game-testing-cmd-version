package diseaseGame.backend.parameters.actions.allActions.closes;

import diseaseGame.backend.parameters.actions.allActions.Action;

public class CloseSportEvents extends Action {
    public CloseSportEvents(){
        this.transferRateBlock = -0.3;
        this.recoverRateBlock = 0.01;
        this.cost = 2;
        this.name = "Uzavretie športových udalostí";
    }
}
