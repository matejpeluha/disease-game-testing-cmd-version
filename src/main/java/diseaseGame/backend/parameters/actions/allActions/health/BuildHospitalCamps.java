package diseaseGame.backend.parameters.actions.allActions.health;

import diseaseGame.backend.parameters.actions.allActions.Action;

public class BuildHospitalCamps extends Action {
    public BuildHospitalCamps(){
        this.transferRateBlock = 0;
        this.recoverRateBlock = 0.1;
        this.cost = 4;
        this.name = "Nemocničné tábory";
    }
}
