package diseaseGame.backend.parameters.actions.allActions.health;

import diseaseGame.backend.parameters.actions.allActions.Action;

public class BuildSpecialHospitals extends Action {
    public BuildSpecialHospitals(){
        this.transferRateBlock = -0.01;
        this.recoverRateBlock = 0.2;
        this.cost = 5;
        this.name = "Špeciálne nemocnice";
    }
}
