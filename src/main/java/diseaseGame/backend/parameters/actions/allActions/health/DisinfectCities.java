package diseaseGame.backend.parameters.actions.allActions.health;

import diseaseGame.backend.parameters.actions.allActions.Action;

public class DisinfectCities extends Action {
    public DisinfectCities(){
        this.transferRateBlock = -0.15;
        this.recoverRateBlock = 0.1;
        this.cost = 2;
        this.name = "Dezinfekcia mestského priestranstva";
    }
}
