package diseaseGame.backend.parameters.actions.allActions.health;

import diseaseGame.backend.parameters.actions.allActions.Action;

public class DrugStoreBoost extends Action {
    public DrugStoreBoost(){
        this.transferRateBlock = 0;
        this.recoverRateBlock = 0.05;
        this.cost = 2;
        this.name = "Karanténa pre ľudí zo zahraničia";
    }
}
