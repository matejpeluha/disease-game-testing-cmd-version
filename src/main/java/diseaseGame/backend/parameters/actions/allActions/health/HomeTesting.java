package diseaseGame.backend.parameters.actions.allActions.health;

import diseaseGame.backend.parameters.actions.allActions.Action;

public class HomeTesting extends Action {
    public HomeTesting(){
        this.transferRateBlock = -0.01;
        this.recoverRateBlock = 0.05;
        this.cost = 2;
        this.name = "Domáce testovanie";
    }
}
