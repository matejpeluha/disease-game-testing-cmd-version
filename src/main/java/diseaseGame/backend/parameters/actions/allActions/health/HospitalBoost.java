package diseaseGame.backend.parameters.actions.allActions.health;

import diseaseGame.backend.parameters.actions.allActions.Action;

public class HospitalBoost extends Action {
    public HospitalBoost(){
        this.transferRateBlock = 0;
        this.recoverRateBlock = 0.1;
        this.cost = 2;
        this.name = "Posilnenie nemocníc";
    }
}
