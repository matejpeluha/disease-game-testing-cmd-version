package diseaseGame.backend.parameters.actions.allActions.health;

import diseaseGame.backend.parameters.actions.allActions.Action;

public class MandatoryMouthCover extends Action {
    public MandatoryMouthCover(){
        this.transferRateBlock = -0.3;
        this.recoverRateBlock = 0;
        this.cost = 2;
        this.name = "Povinné rúška";
    }
}
