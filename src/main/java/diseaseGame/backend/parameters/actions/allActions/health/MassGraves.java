package diseaseGame.backend.parameters.actions.allActions.health;

import diseaseGame.backend.parameters.actions.allActions.Action;

public class MassGraves extends Action {
    public MassGraves(){
        this.transferRateBlock = -0.05;
        this.recoverRateBlock = 0.01;
        this.cost = 2;
        this.name = "Masové hroby";
    }
}
