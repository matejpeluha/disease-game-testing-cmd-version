package diseaseGame.backend.parameters.actions.allActions.health;

import diseaseGame.backend.parameters.actions.allActions.Action;

public class TemperatureMeasuring extends Action {
    public TemperatureMeasuring(){
        this.transferRateBlock = 0.005;
        this.recoverRateBlock = 0.01;
        this.cost = 1;
        this.name = "Meranie teploty na verejnosti";
    }
}
