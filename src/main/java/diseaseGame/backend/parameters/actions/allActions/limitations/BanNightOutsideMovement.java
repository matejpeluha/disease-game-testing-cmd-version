package diseaseGame.backend.parameters.actions.allActions.limitations;

import diseaseGame.backend.parameters.actions.allActions.Action;

public class BanNightOutsideMovement extends Action {
    public BanNightOutsideMovement(){
        this.transferRateBlock = -0.05;
        this.recoverRateBlock = 0;
        this.cost = 5;
        this.name = "Zákaz vychádzania v noci";
    }
}
