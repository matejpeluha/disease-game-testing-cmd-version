package diseaseGame.backend.parameters.actions.allActions.limitations;

import diseaseGame.backend.parameters.actions.allActions.Action;

public class LimitedOutsideMovement extends Action {
    public LimitedOutsideMovement(){
        this.transferRateBlock = -0.6;
        this.recoverRateBlock = 0.03;
        this.cost = 4;
        this.name = "Obmedzenie pohybu";
    }
}
