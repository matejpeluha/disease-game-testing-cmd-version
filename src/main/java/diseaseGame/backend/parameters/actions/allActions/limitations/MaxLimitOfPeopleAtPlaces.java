package diseaseGame.backend.parameters.actions.allActions.limitations;

import diseaseGame.backend.parameters.actions.allActions.Action;

public class MaxLimitOfPeopleAtPlaces extends Action {
    public MaxLimitOfPeopleAtPlaces(){
        this.transferRateBlock = -0.2;
        this.recoverRateBlock = 0;
        this.cost = 1;
        this.name = "Maximálny počet ľudí na jednom mieste";
    }
}
