package diseaseGame.backend.parameters.actions.allActions.limitations;

import diseaseGame.backend.parameters.actions.allActions.Action;

public class QuarantineForIncomers extends Action {
    public QuarantineForIncomers(){
        this.transferRateBlock = -0.1;
        this.recoverRateBlock = 0.05;
        this.cost = 2;
        this.name = "Karanténa pre ľudí zo zahraničia";
    }
}
