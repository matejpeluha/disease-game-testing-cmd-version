package diseaseGame.backend.parameters.actions.allActions.locks;

import diseaseGame.backend.parameters.actions.allActions.Action;

public class LockCities extends Action {
    public LockCities(){
        this.transferRateBlock = -0.5;
        this.recoverRateBlock = 0.05;
        this.cost = 4;
        this.name = "Uzamknutie miest";
    }
}
