package diseaseGame.backend.parameters.actions.allActions.locks;

import diseaseGame.backend.parameters.actions.allActions.Action;

public class LockRetirementHomes extends Action {
    public LockRetirementHomes(){
        this.transferRateBlock = -0.01;
        this.recoverRateBlock = 0.01;
        this.cost = 1;
        this.name = "Uzamknutie domovov dôchodcov";
    }
}
