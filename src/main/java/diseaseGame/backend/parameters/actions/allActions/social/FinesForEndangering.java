package diseaseGame.backend.parameters.actions.allActions.social;

import diseaseGame.backend.parameters.actions.allActions.Action;

public class FinesForEndangering extends Action {
    public FinesForEndangering(){
        this.transferRateBlock = -0.03;
        this.recoverRateBlock = 0.01;
        this.cost = 1;
        this.name = "Pokuty";

    }
}
