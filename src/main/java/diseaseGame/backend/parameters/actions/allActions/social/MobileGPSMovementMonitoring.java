package diseaseGame.backend.parameters.actions.allActions.social;

import diseaseGame.backend.parameters.actions.allActions.Action;

public class MobileGPSMovementMonitoring extends Action {
    public MobileGPSMovementMonitoring(){
        this.transferRateBlock = -0.05;
        this.recoverRateBlock = 0;
        this.cost = 1;
        this.name = "Sledovanie pohybu občanov";
    }
}
