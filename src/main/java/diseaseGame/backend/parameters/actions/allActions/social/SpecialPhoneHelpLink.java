package diseaseGame.backend.parameters.actions.allActions.social;

import diseaseGame.backend.parameters.actions.allActions.Action;

public class SpecialPhoneHelpLink extends Action {
    public SpecialPhoneHelpLink(){
        this.transferRateBlock = -0.01;
        this.recoverRateBlock = 0.05;
        this.cost = 1;
        this.name = "Pomocná telefónna linka";
    }
}
