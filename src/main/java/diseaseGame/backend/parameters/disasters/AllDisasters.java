package diseaseGame.backend.parameters.disasters;

import diseaseGame.backend.parameters.disasters.allDisasters.*;

public class AllDisasters {
    private final Disaster[] allDisasters;

    public AllDisasters(){
        this.allDisasters = new Disaster[]{
                new Protests(),
                new Floods(),
                new BigFire()};
    }

    private boolean isDisasterDaySameAsDay(Disaster disaster, int day) throws NullPointerException{
        if (disaster == null)
            throw new NullPointerException("Chyba v engine. Neplatna katastrofa.");

        return disaster.getDay() == day;
    }

    public Disaster getDisaster(int day){
        for(Disaster disaster : allDisasters){
            if(isDisasterDaySameAsDay(disaster, day))
                return disaster;
        }

        return new NoDisaster();
    }

    public String getDisasterWarning(int day){
        Disaster disaster = getDisaster(day);

        return disaster.getDisasterWarning();
    }

    private void deactivateDisasterIfSameDay(Disaster disaster, int day){
        if(disaster == null)
            throw new NullPointerException("Chyba v backend. Neexistujuca disaster.");

        if(disaster.getDay() == day)
            disaster.deactivate();
    }

    public void deactivateTodayDisaster(int day){
        for(Disaster disaster : allDisasters){
            deactivateDisasterIfSameDay(disaster, day);
        }
    }

}
