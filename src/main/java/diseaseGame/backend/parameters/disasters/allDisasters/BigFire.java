package diseaseGame.backend.parameters.disasters.allDisasters;

public class BigFire extends Disaster{
    public BigFire(){
        this.changeTransferRate = -0.4;
        this.changeRecoverRate = 0.1;
        this.day = 15;
        this.name = "Veľký požiar";
    }
}
