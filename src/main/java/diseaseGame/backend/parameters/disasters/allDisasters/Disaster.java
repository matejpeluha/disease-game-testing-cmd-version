package diseaseGame.backend.parameters.disasters.allDisasters;

import lombok.Getter;

@Getter
abstract public class Disaster {
    protected String name;
    protected double changeTransferRate;
    protected double changeRecoverRate;
    protected int day;
    protected final int COST = 4;
    protected boolean active = true;

    public String getDisasterWarning(){
        return "\nPrihodila sa nasledujúca udalosť: " + name + "\n Na jej zvládnutie zaplať " + COST + " bodov. Použi príkaz yes.\n";
    }

    public void deactivate(){
        this.active = false;
    }

}

