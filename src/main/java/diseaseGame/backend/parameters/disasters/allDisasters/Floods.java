package diseaseGame.backend.parameters.disasters.allDisasters;

public class Floods extends Disaster{
    public Floods(){
        this.changeTransferRate = -0.1;
        this.changeRecoverRate = 0.3;
        this.day = 10;
        this.name = "Záplavy";
    }
}
