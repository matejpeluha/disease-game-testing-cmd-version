package diseaseGame.backend.parameters.disasters.allDisasters;

public class NoDisaster extends Disaster{
    public NoDisaster(){
        this.changeTransferRate = 0;
        this.changeRecoverRate = 0;
        this.day = -1;
        this.name = "";
        this.active = false;
    }

    @Override
    public String getDisasterWarning() {
        return "";
    }
}
