package diseaseGame.backend.parameters.disasters.allDisasters;

public class Protests extends Disaster{
    public Protests(){
        this.changeTransferRate = -0.6;
        this.changeRecoverRate = 0.02;
        this.day = 5;
        this.name = "Protesty";
    }
}
