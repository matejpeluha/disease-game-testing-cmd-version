package diseaseGame.backend.parameters.diseases;

public class Bacteria extends Disease {
    public Bacteria(String name) {
        super(name);
        this.transferRate = 4;
        this.recoverRate = 0.2;
        this.diseaseType = "baktéria";
        calculateR0();
    }

    public void mutate() {
        this.transferRate *= 1.05;
        this.recoverRate *= 0.995;
        calculateR0();
    }

}
