package diseaseGame.backend.parameters.diseases;

import lombok.Getter;

@Getter
abstract public class Disease {
    protected final String name;
    protected double transferRate;
    protected double recoverRate;
    protected double R0;
    protected String diseaseType;

    public Disease(String name){
        this.name = name;
    }

    protected void calculateR0(){
        this.R0 = transferRate / recoverRate;
    }

    public abstract void mutate();

    public void weakenTransferRateBy(double changeTransferRate){
        if(transferRate < changeTransferRate)
            changeTransferRate = transferRate;

        this.transferRate += changeTransferRate;
        calculateR0();
    }

    public void weakenRecoverRateBy(double changeRecoverRate){
        if(this.recoverRate + changeRecoverRate > 1)
            this.recoverRate = 1;
        else
            this.recoverRate += changeRecoverRate;

        calculateR0();
    }

    public void boostTransferRateBy(double changeTransferRate){
        this.transferRate -= changeTransferRate;
        calculateR0();
    }

    public void boostRecoverRateBy(double changeRecoverRate){
        if(recoverRate < changeRecoverRate)
            changeRecoverRate = recoverRate;

        this.recoverRate -= changeRecoverRate;
        calculateR0();
    }

}


