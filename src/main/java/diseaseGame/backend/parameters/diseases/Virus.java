package diseaseGame.backend.parameters.diseases;

public class Virus extends Disease{
    public Virus(String name) {
        super(name);
        this.transferRate = 5;
        this.recoverRate = 0.3;
        this.diseaseType = "vírus";
        calculateR0();
    }

    public void mutate() {
        this.transferRate *= 1.1;
        this.recoverRate *= 0.99;
        calculateR0();
    }
}
