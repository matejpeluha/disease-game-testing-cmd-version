package diseaseGame.frontend;

public class ClearConsole {

    public static void cls()
    {
        try
        {
            new ProcessBuilder("cmd","/c","cls").inheritIO().start().waitFor();
        }
        catch (Exception E)
        {
            System.out.println(E);
        }

    }
}
