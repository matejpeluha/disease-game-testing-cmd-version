package diseaseGame.frontend;

import diseaseGame.backend.SimpleSimulation;
import diseaseGame.backend.Simulation;
import diseaseGame.frontend.gameplay.Gameplay;
import diseaseGame.frontend.gameplay.SimpleGameplay;
import diseaseGame.frontend.gameplay.VaxGameplay;
import diseaseGame.frontend.gameplay.helpers.SimulationSettingProcessPrinter;
import diseaseGame.frontend.settings.SettingChooser;

public class Game {
    private Simulation simulation;

    private void setUpSimulation(){
        SettingChooser chooser = new SettingChooser();

        this.simulation = chooser.createSimulationAndSetUp();
    }

    private void printTutorial(){
        SimulationSettingProcessPrinter printer = new SimulationSettingProcessPrinter();
        printer.printTutorial();
    }

    private Gameplay getCorrectGameplay(){
        if(simulation instanceof SimpleSimulation)
            return new SimpleGameplay(simulation);
        else
            return new VaxGameplay(simulation);
    }

    private void startGame(){
        Gameplay gameplay = getCorrectGameplay();

        gameplay.startGame();
    }

    public void play(){
        printTutorial();
        setUpSimulation();
        startGame();
    }
}
