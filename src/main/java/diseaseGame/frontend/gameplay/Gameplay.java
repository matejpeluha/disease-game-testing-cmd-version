package diseaseGame.frontend.gameplay;

import diseaseGame.backend.Simulation;
import diseaseGame.backend.parameters.actions.allActions.Action;
import diseaseGame.backend.parameters.disasters.allDisasters.Disaster;
import diseaseGame.backend.parameters.diseases.Disease;
import diseaseGame.frontend.ClearConsole;
import diseaseGame.frontend.gameplay.helpers.SimulationUserInterfacePrinter;

import java.util.Scanner;

abstract public class Gameplay {
    protected Simulation simulation;
    protected String message = "";

    public Gameplay(Simulation simulation) throws IllegalArgumentException{
        if(simulation == null)
            throw new IllegalArgumentException("Neexistujuca simulacia. Chyba vo frontende.");

        this.simulation = simulation;
    }

    private void printMessage(){
        System.out.println(message);
    }

    private boolean somebodyIsNOTWinner(){
        return simulation.somebodyIsNOTWinner();
    }

    private void printDisasterWarning(){
        Disaster disaster = simulation.getTodayDisaster();
        String warning = disaster.getDisasterWarning().toUpperCase();

        if(disaster.isActive())
            System.out.println(warning);
    }

    private void printUserInterface(){
        SimulationUserInterfacePrinter printer = new SimulationUserInterfacePrinter();

        printer.printUserInterface(simulation);
        printMessage();
        printDisasterWarning();
    }

    private boolean isPlayerPlayingHisMove(String actionNumber){
        return !actionNumber.equals("");
    }


    private String playerChooseMove(){
        this.message = "";
        System.out.println("\nPre ukoncenie tahu a pokracovanie na novy den stlac IBA ENTER.");
        System.out.print("Pre vyber akcie, zapis cislo akcie ktoru chces vykonat: ");

        Scanner scanner = new Scanner(System.in);

        return scanner.nextLine().toLowerCase();
    }

    private boolean isInteger(String string){
        if(string == null)
            throw new NullPointerException("Neexistujuci tah. Chyba vo frontende.");
        if(string.equals(""))
            return false;

        for(int a = 0; a < string.length(); a++)
        {
            if(a == 0 && string.charAt(a) == '-') continue;
            if( !Character.isDigit(string.charAt(a)) ) return false;
        }
        return true;
    }


    private void setSuccessfulMessage(int actionIndex) throws IllegalAccessException {
        Action action =  simulation.getAction(actionIndex);
        int cost = action.getCost();
        double transferRate = - action.getTransferRateBlock();
        double recoverRate = action.getRecoverRateBlock();
        String recoverRateOperator;
        String otherOperator;

        if(action.isActive()){
            recoverRateOperator = "+";
            otherOperator = "-";
        }
        else{
            recoverRateOperator = "-";
            otherOperator = "+";
        }

        this.message = "Zmeny(transfer/recover/body): " + otherOperator + transferRate + "/"
                + recoverRateOperator + recoverRate + "/" + otherOperator + cost;
    }

    protected void tryPlayersMove(int actionIndex){
        try{
            this.simulation.turnAction(actionIndex);
            setSuccessfulMessage(actionIndex);
        }catch (IllegalAccessException | RuntimeException exception){
            this.message = exception.getMessage();
            doPlayersMove();
        }
    }

    private void dealWithDisasters(String decision){
        try {
            this.simulation.dealWithDisaster(decision);
        }catch (IllegalArgumentException exception){
            doPlayersMove();
        }
    }

    private void tryPlayersMove(String move){
        int intMove;

        if(isInteger(move)) {
            intMove = Integer.parseInt(move);
            tryPlayersMove(intMove);
        }
        else if( isPlayerPlayingHisMove(move) )
            dealWithDisasters(move);
    }

    protected void doPlayersMove(){
        String move;

        do{
            printUserInterface();
            move = playerChooseMove();
            tryPlayersMove(move);
        }while(isPlayerPlayingHisMove(move));
    }

    private void nextDay(){
        this.simulation.nextDay();
    }

    private void decideAndPrintWinner(){
        ClearConsole.cls();
        Disease disease = simulation.getDisease();

        if (simulation.isPlayerWinner())
            System.out.println("Hrac vyhral nad " + disease.getDiseaseType() + " " + disease.getName());
        else if(simulation.isDiseaseWinner())
            System.out.println(disease.getDiseaseType() + " " + disease.getName() + " vyhralo na hracom.");
    }

    public void startGame(){
        while(somebodyIsNOTWinner()){
            doPlayersMove();
            nextDay();
        }

        decideAndPrintWinner();
    }

}
