package diseaseGame.frontend.gameplay;

import com.sun.corba.se.impl.io.TypeMismatchException;
import diseaseGame.backend.Simulation;
import diseaseGame.backend.VaxSimulation;

public class VaxGameplay extends Gameplay{
    public VaxGameplay(Simulation simulation) {
        super(simulation);
    }

    private void upgradeVax() throws IllegalAccessException {
        if(simulation instanceof VaxSimulation)
            ((VaxSimulation)simulation).levelUpVaxRate();
        else
            throw new TypeMismatchException("Zly datovy typ simulacie. Chyba vo frontende");
    }

    public void setVaxSuccessfulMessage(){
        int vaxRate;

        if(simulation instanceof VaxSimulation)
            vaxRate =((VaxSimulation)simulation).getVaxingPerDay();
        else
            throw new TypeMismatchException("Zly datovy typ simulacie. Chyba vo frontende");

        this.message = "Počet očkovaných každý deň: " + vaxRate;
    }

    private void tryUpgradeVax(){
        try{
            upgradeVax();
            setVaxSuccessfulMessage();
        }catch (IllegalAccessException | RuntimeException exception ){
            this.message = exception.getMessage();
            doPlayersMove();
        }
    }

    @Override
    protected void tryPlayersMove(int moveIndex) {
        if(moveIndex == -1)
            tryUpgradeVax();
        else
            super.tryPlayersMove(moveIndex);
    }
}
