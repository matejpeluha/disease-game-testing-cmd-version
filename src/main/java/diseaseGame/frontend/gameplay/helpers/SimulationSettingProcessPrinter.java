package diseaseGame.frontend.gameplay.helpers;

import diseaseGame.backend.Simulation;
import diseaseGame.backend.parameters.Population;
import diseaseGame.backend.parameters.diseases.Disease;
import diseaseGame.frontend.ClearConsole;

import java.util.Scanner;

public class SimulationSettingProcessPrinter {
    public void printTutorial(){
        ClearConsole.cls();
        System.out.println("Ujal si sa úlohy vlády bojujúcej s vážnym ochorením.\n" +
                "Tvoja úloha je bojovať s ním pomocou rôznych opatrení a vývojom očkovania.\n" +
                "Každé opatrenia stojí nejaký počet akčných bodov. Na začiatku dňa ti pribudnú nové.\n" +
                "Jednotlivé opatrenia oslabujú chorobu. Keď ťa k tomu hra vyzve, opatrenie kúpiš napísaním čísla opatrenia.\n" +
                "Taktiež môžeš zapnuté opatrenie vypnúť a získať tak akčné body späť, rovnako ale posilníš aj chorobu.\n" +
                "Pokiaľ už nechces nakupovať, stlač enter bez zadania čísla.\n" +
                "Mysli na to, že vírus je každým dňom silnejší.\n" +
                "Vyhráš vtedy, pokiaľ faktor R0(transferRate/recoverRate) klesne pod 1. Vtedy je ochorenie pod kontrolou.\n" +
                "Ochorenie vyhrá ak ti nakazí aspoň 90% populácie.\n" +
                "Hra je mierumilovná a vyliečených, zaočkovaných a mŕtvych berie do jednej kategórie, ktorá už vírus nemôže roznásať.\n" +
                "Ak si pripravený hrať, stlač ENTER.");

        Scanner scanner = new Scanner(System.in);
        scanner.nextLine();
    }

    public void printSimulationStartStats(Simulation simulation){
        ClearConsole.cls();

        Disease defaultDisease = simulation.getDisease();
        Population defaultPopulation = simulation.getPopulation();

        System.out.println("Defaultné nastavenia hry: ");
        System.out.println("Typ ochorenia: " + defaultDisease.getDiseaseType());
        System.out.println("Meno ochorenia: " + defaultDisease.getName());
        System.out.println("Veľkosť populácie: " + defaultPopulation.getPopulationSize());
    }

}
