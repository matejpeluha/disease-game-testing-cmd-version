package diseaseGame.frontend.gameplay.helpers;

import diseaseGame.backend.SimpleSimulation;
import diseaseGame.backend.Simulation;
import diseaseGame.backend.VaxSimulation;
import diseaseGame.backend.parameters.Date;
import diseaseGame.backend.parameters.Population;
import diseaseGame.backend.parameters.actions.AllActions;
import diseaseGame.backend.parameters.actions.allActions.Action;
import diseaseGame.backend.parameters.diseases.Disease;
import diseaseGame.frontend.ClearConsole;


public class SimulationUserInterfacePrinter {

    private void throwExceptionIfObjectNull(Object object){
        if(object == null)
            throw new NullPointerException("Chýbajúci parameter. Chyba vo frontende.");
    }

    private void printPopulationStats(Simulation simulation){
        Population population = simulation.getPopulation();
        long susceptible = population.getSusceptible();
        long infectious = population.getInfectious();
        long recovered = population.getRecovered();
        double infectedRate = population.getAllInfectedRate() * 100;

        System.out.println("\nSIR-model\n     Zdraví: " + susceptible + "\n     Nakazení: " + infectious +
                        "\n     Mŕtvi a imúnni:" + recovered + "\n     Percento nakazených dokopy: " + infectedRate + "%");
    }

    private void printDiseaseStats(Simulation simulation){
        Disease disease = simulation.getDisease();
        String type = disease.getDiseaseType();
        String name = disease.getName();
        double transferRate = disease.getTransferRate();
        double recoverRate = disease.getRecoverRate();
        double R0 = disease.getR0();

        System.out.println(type + " " + name + "(choroba aj mutuje):");
        System.out.println("     Transfer rate: " + transferRate + "\n     Recover rate: " +
                100 * recoverRate + "%\n     Priemerný prenos z 1 osoby: " + R0 + " ľudí");
    }

    private void printStats(Simulation simulation){
        printPopulationStats(simulation);
        printDiseaseStats(simulation);
    }

    private String getFormattedActionName(Action action){
        throwExceptionIfObjectNull(action);

        String actionName = action.getName();

        if(action.isActive())
            return actionName.toUpperCase();
        else
            return actionName.toLowerCase();
    }

    private void printOneActionWithId(Action action, int id){
        throwExceptionIfObjectNull(action);

        String actionName = getFormattedActionName(action);
        int cost = action.getCost();

        System.out.println(id + "." + actionName + "(" + cost + "b.)");
    }

    private void printChosenActions(Action[] actions){
        throwExceptionIfObjectNull(actions);

        for(int id = 0; id < actions.length; id++){
            printOneActionWithId(actions[id], id);
        }
    }

    private void printAllActions(AllActions allActions){
        throwExceptionIfObjectNull(allActions);

        Action[] actions = allActions.getAllActions();

        System.out.println("neaktívne/AKTÍVNE opatrenia(cena)");
        printChosenActions(actions);
    }

    private void printAllActions(Simulation simulation){
        AllActions allActions = simulation.getAllActions();

        printAllActions(allActions);
    }

    private void printDate(Date date){
        throwExceptionIfObjectNull(date);

        int day = date.getDay();
        int month = date.getMonth();
        int year = date.getYear();
        int diseaseDay = date.getDiseaseActiveDay();

        System.out.println("Dátum: " + day + "." + month + "." + year);
        System.out.println("Deň ochorenia: " + diseaseDay + ". deň");
    }

    private void printAmountOfActionPointsInSimulation(Simulation simulation){
        int actionPoints = simulation.getActualPoints();
        System.out.println("\nAkčné body: " + actionPoints);
    }

    private void printDayInfo(Simulation simulation){
        Date date = simulation.getDate();
        printDate(date);
    }

    private void printSimpleUserInterface(Simulation simulation){
        printDayInfo(simulation);
        printStats(simulation);
        printAmountOfActionPointsInSimulation(simulation);
        printAllActions(simulation);
    }

    private void printVaxInfo(VaxSimulation simulation){
        int vaxRate = simulation.getVaxingPerDay();
        int vaxCost = simulation.getCostOfNextVaxLevel();

        System.out.println("-1.Vakcína (aktuálne očkovaní každý deň: " + vaxRate + "): " + vaxCost + "b.");
    }

    private void printVaxUserInterface(VaxSimulation simulation){
        printSimpleUserInterface(simulation);
        printVaxInfo(simulation);
    }

    public void printUserInterface(Simulation simulation){
        throwExceptionIfObjectNull(simulation);

        ClearConsole.cls();

        if(simulation instanceof SimpleSimulation)
            printSimpleUserInterface(simulation);
        else if(simulation instanceof VaxSimulation)
            printVaxUserInterface((VaxSimulation) simulation);
    }


}
