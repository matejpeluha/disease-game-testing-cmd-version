package diseaseGame.frontend.settings;


import diseaseGame.backend.Simulation;
import diseaseGame.backend.parameters.Population;
import diseaseGame.backend.parameters.diseases.Disease;
import diseaseGame.frontend.gameplay.helpers.SimulationSettingProcessPrinter;
import diseaseGame.frontend.settings.creators.DiseaseCreator;
import diseaseGame.frontend.settings.creators.PopulationCreator;
import diseaseGame.frontend.settings.creators.SimulationCreator;

import java.util.Scanner;

public class SettingChooser {

    private Simulation createDefaultSimulation(){
        SimulationCreator creator = new SimulationCreator();

        return creator.createDefaultSimulation();
    }

    private void setUpDisease(Simulation simulation){
        DiseaseCreator creator = new DiseaseCreator();
        Disease disease = creator.createDisease();

        simulation.setDisease(disease);
    }

    private void setUpPopulation(Simulation simulation){
        PopulationCreator creator = new PopulationCreator();
        Population population = creator.createPopulation();

        simulation.setPopulation(population);
    }

    private Simulation setUpSimulation(Simulation simulation){
        setUpDisease(simulation);
        setUpPopulation(simulation);

        return simulation;
    }

    private void printSimulationDefaultStats(Simulation simulation){
        SimulationSettingProcessPrinter printer = new SimulationSettingProcessPrinter();

        printer.printSimulationStartStats(simulation);
    }

    private String getPlayersDecisionOnKeepingDefaultSetUp(Simulation simulation){
        Scanner scanner = new Scanner(System.in);

        printSimulationDefaultStats(simulation);
        System.out.println("Prajete si tieto nastavenia zmeniť?(yes/no): ");

        return scanner.nextLine();
    }

    private boolean convertStringDecisionToBoolean(String decision, Simulation simulation){
        if(decision.equals("yes"))
            return true;
        else if(decision.equals("no"))
            return false;
        else
            return doesPlayerWantToChangeDefaultSetUp(simulation);
    }

    private boolean doesPlayerWantToChangeDefaultSetUp(Simulation simulation){

        String decision = getPlayersDecisionOnKeepingDefaultSetUp(simulation);

        return convertStringDecisionToBoolean(decision, simulation);
    }

    public Simulation createSimulationAndSetUp(){
        Simulation simulation = createDefaultSimulation();

        if( doesPlayerWantToChangeDefaultSetUp(simulation) ) {
            setUpSimulation(simulation);
        }

        return simulation;
    }
}
