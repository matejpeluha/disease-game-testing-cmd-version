package diseaseGame.frontend.settings.creators;

import diseaseGame.backend.parameters.diseases.Bacteria;
import diseaseGame.backend.parameters.diseases.Disease;
import diseaseGame.backend.parameters.diseases.Virus;
import diseaseGame.frontend.ClearConsole;

import java.util.Scanner;

public class DiseaseCreator {
    private final String diseaseType1 = "virus";
    private final String diseaseType2 = "bacteria";

    private void printChoiceText(){
        ClearConsole.cls();

        System.out.println("1. " + diseaseType1 + "\n2. " + diseaseType2);
        System.out.println("Zvoľte si typ ochorenia(" + diseaseType1 + "/" + diseaseType2 + "): ");
    }

    private String getClientChoice(){
        Scanner scanner = new Scanner(System.in);

        printChoiceText();

        String diseaseType = scanner.nextLine();

        return diseaseType.toLowerCase();
    }

    private Disease createDiseaseByChoice(String name){
        String diseaseType = getClientChoice();

        if(diseaseType.equals(diseaseType1))
            return new Virus(name);
        else if(diseaseType.equals(diseaseType2))
            return new Bacteria(name);
        else
            return createDiseaseByChoice(name);

    }

    private String chooseDiseaseName(){
        ClearConsole.cls();
        Scanner scanner = new Scanner(System.in);

        System.out.println("Zvol si meno ochorenia.");

        return scanner.nextLine();
    }

    public Disease createDisease(){
        String name = chooseDiseaseName();

        return createDiseaseByChoice(name);
    }

}
