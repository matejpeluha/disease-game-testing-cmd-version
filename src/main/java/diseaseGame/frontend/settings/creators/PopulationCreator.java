package diseaseGame.frontend.settings.creators;

import diseaseGame.backend.parameters.Population;
import diseaseGame.frontend.ClearConsole;

import java.util.InputMismatchException;
import java.util.Scanner;

public class PopulationCreator {

    private void printChoiceText(){
        ClearConsole.cls();

        System.out.println("Zvoľte si veľkosť populácie(min. " + Population.MIN_POPULATION_SIZE + ", max. " + Long.MAX_VALUE + "): ");
    }

    private long choosePopulationSize(){
        Scanner scanner = new Scanner(System.in);

        printChoiceText();

        try {
            return scanner.nextLong();
        }catch(InputMismatchException exception){
            return choosePopulationSize();
        }
    }

    public Population createPopulation(){
         long populationSize = choosePopulationSize();

         try{
             return new Population(populationSize);
         }catch (IllegalArgumentException exception){
             return createPopulation();
         }
    }
}
