package diseaseGame.frontend.settings.creators;

import diseaseGame.backend.SimpleSimulation;
import diseaseGame.backend.Simulation;
import diseaseGame.backend.VaxSimulation;
import diseaseGame.frontend.ClearConsole;

import java.util.Scanner;

public class SimulationCreator {
    private final String simulationType1 = "simple";
    private final String simulationType2 = "vax";


    private void printChoiceText(){
        ClearConsole.cls();

        System.out.println(simulationType1 + " - hra bez očkovania\n" + simulationType2 + " - hra s očkovaním\n");
        System.out.println("Zvoľte si typ hry(" + simulationType1 + "/" + simulationType2 + "): ");
    }

    private String getClientChoice(){
        Scanner scanner = new Scanner(System.in);

        printChoiceText();

        String simulationType = scanner.nextLine();

        return simulationType.toLowerCase();
    }


    private Simulation chooseTypeOfSimulation(){
        String simulationType = getClientChoice();

        if(simulationType.equals(simulationType1))
            return new SimpleSimulation();
        else if(simulationType.equals(simulationType2))
            return new VaxSimulation();
        else
            return chooseTypeOfSimulation();
    }

    public Simulation createDefaultSimulation(){
        return chooseTypeOfSimulation();
    }
}
